alias j="jobs"
alias pd="pushd"
alias cls="clear"
alias svim="sudo vim"
alias fsudo='sudo $(fc -ln -1)'
alias tvim='vim --servername tvim --remote-tab-silent '
